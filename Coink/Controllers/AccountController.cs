using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Coink.Data;
using Coink.Dtos;
using Coink.Entities;
using Coink.IRepository;
using Coink.IService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Coink.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly DataContext _context;
        private readonly ITokenService _tokenService;
        private readonly IValidatePlaceResidence _validatePlaceResidence;
        private readonly IAccountRepository _accountRepository;
        
        public AccountController(DataContext context, ITokenService tokenService, 
            IValidatePlaceResidence validatePlaceResidence, IAccountRepository accountRepository)
        {
            _context = context;
            _tokenService = tokenService;
            _validatePlaceResidence = validatePlaceResidence;
            _accountRepository = accountRepository;
        }
        
        [HttpPost("register")]
        public async Task<ActionResult<UserDto>> Register(RegisterDto registerDto)
        {
            if (await UserExists(registerDto.Username)) return BadRequest("El usuario ya existe");

            var resultValidateResidence = await _validatePlaceResidence
                .ValidateResidence(registerDto.IdCountry, registerDto.IdDepartment, registerDto.IdTown);

            if (!resultValidateResidence)
                return BadRequest("El lugar de residencia ingresado no se encuentra en nuestra base de datos");

            // agregar la validacion del municipio 
            
            using var hmac = new HMACSHA512();

            CreateUserDto user = new CreateUserDto
            {
                UserName = registerDto.Username.ToLower(),
                PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(registerDto.Password)),
                PasswordSalt = hmac.Key,
                Name = registerDto.Name,
                Telephone = registerDto.Telephone,
                Address = registerDto.Address,
                Town = registerDto.IdTown
            };

            var result =  _accountRepository.SaveUser(user);

            if (result != null) 
                return Ok($"El Usuario {result.UserName} fue creado correctamente");
            else
                return BadRequest("Ocurrio un error, intentalo nuevamente");
        }
        
        private async Task<bool> UserExists(string username) 
        {
            return await _context.Users.AnyAsync(x => x.UserName == username.ToLower());
        }

    }
}