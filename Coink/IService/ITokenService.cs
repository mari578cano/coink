using Coink.Entities;

namespace Coink.IService
{
    public interface ITokenService
    {
        string CreateToken(Users user);
    }
}