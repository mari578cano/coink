using System.Threading.Tasks;
using Coink.Dtos;

namespace Coink.IService
{
    public interface IValidatePlaceResidence
    {
        Task<bool> ValidateResidence(int country, int department, int town);
    }
}