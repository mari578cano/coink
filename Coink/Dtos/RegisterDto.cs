using System.ComponentModel.DataAnnotations;

namespace Coink.Dtos
{
    public class RegisterDto
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [StringLength(8, MinimumLength = 4)]
        public string Password { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Telephone { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public int IdCountry { get; set; }
        [Required]
        public int IdDepartment { get; set; }
        [Required]
        public int IdTown { get; set; }
    }
}