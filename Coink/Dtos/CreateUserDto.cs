using System.ComponentModel.DataAnnotations;

namespace Coink.Dtos
{
    public class CreateUserDto
    {
        public string UserName { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        
        public int IdCountry { get; set; }
        
        public int IdDepartment { get; set; }
        public int Town { get; set; }
    }
}