namespace Coink.Dtos
{
    public class PlaceResidenceDto
    {
        public string Country { get; set; }
        public string Department { get; set; }
        public string Town { get; set; }
    }
}