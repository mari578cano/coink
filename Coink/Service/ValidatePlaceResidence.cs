using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Coink.Data;
using Coink.Dtos;
using Coink.IService;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Coink.Service
{
    public class ValidatePlaceResidence : IValidatePlaceResidence
    {
        private readonly DataContext _context;
        
        public ValidatePlaceResidence(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> ValidateResidence(int country, int department, int town)
        {
            try
            {
                PlaceResidenceDto validate = new PlaceResidenceDto();
                string sql = $"exec findDataLocation {@country}, {@department}, {@town}";

                var result = _context.Towns.FromSqlRaw(sql).ToList();
                
                return result.Any() ? true : false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        
    }
}