using System.ComponentModel.DataAnnotations;

namespace Coink.Entities
{
    public class Town
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int IdDepartment { get; set; }
    }
}