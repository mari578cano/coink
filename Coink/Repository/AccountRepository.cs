using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Coink.Data;
using Coink.Dtos;
using Coink.Entities;
using Coink.IRepository;
using Microsoft.EntityFrameworkCore;

namespace Coink.Repository
{
    public class AccountRepository : IAccountRepository
    {
        private readonly DataContext _context;
        
        public AccountRepository(DataContext context)
        {
            _context = context;
        }

        public Users SaveUser(CreateUserDto registerDto)
        {
            try
            {
                string sql = $"exec registerUser {@registerDto.UserName}, {@"0x" + BitConverter.ToString(registerDto.PasswordHash).Replace("-", "")}, {@"0x" + BitConverter.ToString(registerDto.PasswordSalt).Replace("-", "")}, {@registerDto.Name}, {@registerDto.Telephone}, {@registerDto.Address}, {@registerDto.Town}";

                var result = _context.Users.FromSqlRaw(sql).ToList().First();
                
                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

    }
}