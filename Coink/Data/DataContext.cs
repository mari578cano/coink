using Coink.Entities;
using Microsoft.EntityFrameworkCore;

namespace Coink.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }
        
        public DbSet<Users> Users { get; set; }
        public DbSet<Town> Towns { get; set; }
    }

}