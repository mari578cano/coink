using System.Collections.Generic;
using System.Threading.Tasks;
using Coink.Dtos;
using Coink.Entities;

namespace Coink.IRepository
{
    public interface IAccountRepository
    {
        Users SaveUser(CreateUserDto registerDto);
    }
}